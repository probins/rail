const extent = ol.proj.transformExtent([-6.567327,49.78,2.133845,59.1524], 'EPSG:4326','EPSG:3857');

let map = new ol.Map({
  layers: [
    new ol.layer.Tile({
        source: new ol.source.OSM()
    }),
    new ol.layer.VectorTile({
      source: new ol.source.VectorTile({
        //   attributions: '',
        renderMode: 'vector',
        format: new ol.format.MVT(),
        minZoom: 6,
        maxZoom: 10,
        url: '../tracktiles/{z}/{x}/{y}.pbf'
      }),
      style: getStyle
    })//,
    // new ol.layer.VectorTile({
    //   source: new ol.source.VectorTile({
    //     //   attributions: '',
    //     renderMode: 'vector',
    //     format: new ol.format.MVT(),
    //     minZoom: 8,
    //     maxZoom: 8,
    //     url: '../stationtiles/{z}/{x}/{y}.pbf'
    //   }),
    //   style: getStyle
    // })
  ],
  target: 'map',
  view: new ol.View({
    center: ol.proj.fromLonLat([-2, 52]),
    zoom: 8,
    minZoom: 6,
    maxZoom: 16,
    extent: extent
  })
});

function getStyle(feature) {
  let color = 'orange';
  let elec = feature.getProperties().electrified;
  if (elec == 'rail') {
    color = 'cyan';
  }
  if (elec == 'contact_line') {
    if (feature.getProperties().voltage == '1500') {
      color = 'greenyellow';
    } else {
      color = 'lime';
    }
  }
  if (feature.getProperties()["railway:traffic_mode"] == 'freight') {
    color = 'royalblue';
  }
  let styles = {
    'Point': new ol.style.Style({
      image: new ol.style.Circle({
        radius: 4,
        fill: null,
        stroke: new ol.style.Stroke({color: 'red', width: 1})
      })
    }),
    'LineString': new ol.style.Style({
      stroke: new ol.style.Stroke({
        color: color,
        width: 3
      })
    }),
    'MultiLineString': new ol.style.Style({
      stroke: new ol.style.Stroke({
        color: color,
        width: 3
      })
    })
  };
//    var text = new ol.style.Style({text: new ol.style.Text({
//        text: 'Hello', fill: fill, stroke: stroke
//    })});
  return styles[feature.getGeometry().getType()];
}

map.on('click', event => {
  let features = map.getFeaturesAtPixel(event.pixel);
  if (!features || !features.length) {
    return;
  }
  let feature = features[0];
  if (!feature) {
    return;
  }
  let op = '';
  for (let [key, value] of Object.entries(feature.getProperties())) {
    op = op + `${key}: ${value}\n`;
  }
  alert(op);
});
