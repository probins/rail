### Passenger Rail decarbonisation

#### Aim
Monitor progress of phasing out diesel on passenger services, planned by 2040 or earlier if possible.

#### Directory structure
`nr` and `osm` are archives of material used in setting up the site. `src` is the source for the text pages, converted into HTML by [Lume](https://oscarotero.github.io/lume/); `_config.js` is the config file for this. `tracks` contains the map data and scripts.

#### Programs/scripts
The scripts used to create the map are all in JavaScript. Some of the older ones run with NodeJS, but the newer ones use Deno. All map data are in GeoJSON format, as LineStrings, with configuration files in JSON. The LineStrings all have a unique id, though the ELR, in the `name` property, is the identifier used in the system. The map page itself uses [OpenLayers](https://openlayers.org/) for displaying the data on the base map.

See the Readme in `nr/tracks` for more info on how the original map was created.

#### Maintaining the map
See the Updating page for common changes and which config files they affect. Not included in this are new lines, though this doesn't happen very often; depending on where the data come from, may need to truncate coords, remove MultiLineStrings, rename properties, simplify coords. Small changes, for example, where a track is straightened or moved slightly, do not affect the overall picture, so can generally be ignored.

How the data are displayed is controlled by the JSON configuration files in `config/`, one for each year. These are processed by `createData.js`. There is also a `FeatureCollection.js` Class for manipulating GeoJSON FeatureCollections.

The starting place is the base file of data supplied by NR, `tracks-simp.geojson` (see the Readme in `nr/tracks/` for more info on how this was created). `process.js` takes this, and:
- split tracks defined in `splits.json` (part-frt or part-electrified). New ids/names will be created, for example, ELR 'ABC' would be split into 'ABC-0' and 'ABC-1'. So, if 'ABC-1' is electrified, this must be added to `adjustElecs.json`, not 'ABC' which no longer exists
- add electrified tracks in `electrified.json`
- add new electrification in `adjustelecs.json`
- add battery capability as defined in `battery.json`
- uses `closed.json`, `freight.json`, `mains.json`, `reopens.json` to split the tracks into separate output files in `public/data/`: `tracks_closed.geojson` etc, plus `tracks_rest.geojson` (those not in any of the other categories)

As track is electrified, it can be moved from `adjustElecs.json` files to `electrified.json`.

Most re-openings are freight or closed lines that are already in the system, just not with passenger service. In other words, the way the data are displayed is changed. They would only have to be added if they were closed so long ago they were not included in the NR data, in which case they can be handled as new routes.

Changes can be made using GitLab's editor, and then committed, or they can be added and committed locally in Git, and pushed to the GitLab repo.

Originally, the site used Gitlab Pages, as defined in `.gitlab-ci.yml`. It now uses Cloudflare Pages, and the deploy command is defined there, and run automatically on every commit. This creates the files and copies them to the website.
