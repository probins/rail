/**
 * Module for creating track data, which can be imported from a
 * command-line utility or static site generator.
 *
 * Exports a function to create data
 *
 * filenames/paths defined in config.json
 * Takes the output from the original NR tracks process (tracks-simp.geojson -
 * see nr/tracks/), and the various config files, and:
 * - splits tracks defined in splits.json (part-frt or part-electrified)
 * - add additional electrified track in electrified.json (up to current)
 * - adds new electrified/WIP for each time period in adjustelecs.json
 * - adds battery capability as defined in batteryLines.json
 * - splits tracks into output files: freight, closed, electrified
 * and unelectrified (all others)
 * - outputs to the website directory tracks/:
 * -- freight/closed only one file
 * -- electrified one per year; unelectrified one for each battery range
*/

import FeatureCollection from "./FeatureCollection.js";
import nearest from "https://jspm.dev/npm:@turf/nearest-point-on-line@6.3.0";
import lineSplit from "https://jspm.dev/npm:@turf/line-split@6.3.0";

// config files
import config from "./config.json" assert { type: "json" };

import mainObj from "./config/mains.json" assert { type: "json" };
import lines from "./config/batteryLines.json" assert { type: "json" };
import splits from "./config/splits.json" assert { type: "json" };
import electrified from "./config/electrified.json" assert { type: "json" };
import adjustElecs from "./config/adjustelecs.json" assert { type: "json" };
import freight from "./config/freight.json" assert { type: "json" };
import closed from "./config/closed.json" assert { type: "json" };
import reopens from "./config/reopens.json" assert { type: "json" };

// separate files for different years/battery ranges
const years = config.years,
    ranges = config.ranges;

let tandb = [],
    oneway = [],
    neither = [],
    neither1 = [];

// elr-title xref
let titles = new Map();

// set batteryLines titles
for (const prop in lines) {
  const props = lines[prop];
  // add titles
  props[0].forEach(elr => {
    titles.set(elr, prop);
  });
}

// combine mains arrays and set mains titles
let mains = concatMains();

// put all mains arrays together
function concatMains() {
  let mains = [];
  for (const prop in mainObj) {
    // add titles
    mainObj[prop].forEach(elr => {
      titles.set(elr, prop);
    });
    // concatenate ELRs
    mains = mains.concat(mainObj[prop]);
  }
  return mains;
}

function splitTracks(features) {
  // process tracks to be split
  let opFeatures = [], splitFid = 1600;
  // object listing ELRs with coords and mileage at split point
  for (const property in splits) {
    let code = property, parms = splits[property];

    parms.forEach((rest, i) => {
      let track;
      if (i == 0) {
        let index = features.findIndex(t => t.properties.name == code);
        track = features[index];
        features.splice(index, 1); // remove track being split
      } else {
        // if not first, amend last feature
        track = opFeatures.pop();
      }
      split(track, rest);
    });

    function split(track, rest) {
      let coords = track.geometry.coordinates;
      let parts = splitTrack(track, rest[0]);
      parts.forEach((part, i) => {
        let props = {...track.properties}; // create new object to prevent closure
        // suffix with -
        if (props.name.indexOf('-') == -1) {
          // not already split
          props.name = props.name + '-' + i;
        } else {
          // already split, with suffix
          let [re, suffix] = props.name.split('-');
          suffix = parseInt(suffix);
          suffix = (i == 0) ? suffix : suffix+1;
          props.name = `${re}-${suffix}`;
        }
        if (i == 0) {
          props.endMile = rest[1];
        } else {
          props.startMile = rest[1];
        }
        let outFeature = {
          "type": "Feature",
          "id": splitFid,
          "geometry": { type: "LineString", coordinates: part},
          properties: props
        };
        splitFid++;
        opFeatures.push(outFeature);
      });

      function splitTrack(line, splitCoord) {
        let point = {
            "type": "Point",
            "coordinates": splitCoord
        };
        let found = nearest(line, point);
        let splitted = lineSplit(line, found);
        let [part1, part2] = [splitted.features[0].geometry.coordinates, splitted.features[1].geometry.coordinates];

        return [part1, part2];
      }
    }
  }

  return opFeatures;
}

// for each batteryLine, add to tandb, oneway, neither as appropriate
function calculateBatteryRanges(batteryRange) {
  const thereandback = batteryRange/2;

  for (const prop in lines) {
    const props = lines[prop];
    // if line length longer than battery range, ignore
    if (props[1] < batteryRange) {
      // elec at both ends
      if (props[2] == 'both') {
        tandb = tandb.concat(props[0]);
      // elec at one end
      } else if (props[2] == 'one') {
        if (props[1] < thereandback) {
          tandb = tandb.concat(props[0]);
        } else {
          oneway = oneway.concat(props[0]);
        }
      // no elec but ok if recharging
      } else if (props[1] < thereandback) {
        neither = neither.concat(props[0]);
      } else {
        neither1 = neither1.concat(props[0]);
      }
    }
  }
}

export default function() {
  // Tracks input file (looks like JSON modules can't handle geojson)
  let tracks = JSON.parse(Deno.readTextFileSync(config.inFile)).features;
  // split tracks as defined in splits.json and add to existing features
  tracks = tracks.concat(splitTracks(tracks));

  // add electrified type (L & T)
  addElr('L', electrified);
  addElr('T', electrified);

  let frtFC = new FeatureCollection('frt');
  let closedFC = new FeatureCollection('closed');

  // output freight and closed files (no elec)
  tracks.forEach((track) => {
    let props = track.properties;
    // might as well add mains here
    props.main = mains.includes(props.name) ? 'y' : 'n';

    if (freight.includes(props.name)) {
      frtFC.addFeature(track);
    }
    if (closed[props.name]) {
      closedFC.addFeature(track);
    }
  });

  const opFiles = config.opFiles,
      tracksDir = config.opDir;

  frtFC.writeFile(tracksDir+opFiles.frt);
  closedFC.writeFile(tracksDir+opFiles.closed);

  // output electrified file for each year defined in config
  years.forEach(option => {
    // now adjustElecs.json
    Object.entries(adjustElecs).forEach(([key, value]) => {
      if (key != option) {
        if (key < option) {
          // complete
          addElr('L', {"L":adjustElecs[key]});
        } else {
          // not yet electrified
          addElr('n', {"n":adjustElecs[key]});
        }
      }
    });
    // set this year's entries to WIP
    addElr('WIP', {"WIP": adjustElecs[option]});

    // output electrified file
    let electFC = new FeatureCollection('elect');

    // tracks with elecType property
    tracks.forEach((track) => {
      let props = track.properties;

      if (props.elecType !== '-') {
        // clone, so can alter without changing original
        let feature = JSON.parse(JSON.stringify(track));
        // set name to title if present
        if (titles.has(props.name)) {
          feature.properties.elr = props.name;
          feature.properties.name = titles.get(props.name);
        }
        electFC.addFeature(feature);
      }
    });

    electFC.writeFile(tracksDir+option+opFiles.elect);
  });

  // output battery file for each range defined in config
  ranges.forEach(range => {
    calculateBatteryRanges(range);

    let restFC = new FeatureCollection('rest');

    // tracks without elecType property
    tracks.forEach((track) => {
      let props = track.properties;
      // ignore freight/closed
      if (!freight.includes(props.name) && !closed[props.name]) {
        // battery for unelectrified lines?
        // set battery if unelectrified
        if (props.elecType == '-') {
          if (tandb.includes(props.name)) {
            props.battery = 'b';
          }
          if (oneway.includes(props.name)) {
            props.battery = 'o';
          }
          if (neither.includes(props.name || neither1.includes(props.name))) {
            props.battery = 'n';
          }
        }
        if (props.elecType == '-') {
          // clone, so can alter without changing original
          let feature = JSON.parse(JSON.stringify(track));
          // set name to title if present
          if (reopens.includes(props.name)) {
            feature.properties.elecType = 'r';
          }
          if (titles.has(props.name)) {
            feature.properties.elr = props.name;
            feature.properties.name = titles.get(props.name);
          }
          restFC.addFeature(feature);
        }
      }
    });

    restFC.writeFile(tracksDir+range+'km'+opFiles.rest);
  });

  function addElr(electType, object) {
    let elrs = [];
    for (const prop in object[electType]) {
      elrs = elrs.concat(object[electType][prop]);
    }
    elrs.forEach(elr => {
      // update track with appropriate elect type
      let track = tracks.find(x => elr == x.properties.name);
      track.properties.elecType = electType;
      // addElecType(elr, electType);
    });
    return elrs;
  }
}
