---
title: Conclusions
name: conclusions
edited: August 2020
---
* Diesel is not a long-term solution. Given the 30+-year lifespan of a train, meaning a train bought now will still be around well past 2040, there should be a moratorium on all new diesel trains, including diesel bi-modes, though engines in trains with significant remaining life can and should be replaced by new ones with lower emissions (and fuel savings), where there is no immediate prospect of an electric alternative.
* Those existing diesel trains at/near end of life should be the priority for replacement.
* The main current contenders for replacing diesel are batteries, where the unelectrified track is within their range, and hydrogen for longer distances.
* From recent statements from manufacturers, range is taken to currently be around 100km, i.e. 100km between electrified tracks, 50km there + 50km back, or 100km where recharging is available at the end; recharge is taken to be around 10 minutes, so is suitable at a terminus or other place where the train sits for that long.
* Those electric trains with years of life left which are not currently in service should have batteries added and be put back into service; this should be cheaper and faster than buying new trains, even if range and maximum speed may be lower.
* Manufacturers should supply key data on range and maximum speed, plus recharge time for batteries, for (a) refurbishing existing diesel stock, (b) adding batteries or hydrogen to existing electric stock, and (c) producing new stock; they should also provide estimates for how quickly this can be done.
* For decarbonisation, priority should be given to the most populated areas first.
* Main lines (those with heavy usage) and busy suburban lines should in the long term be electrified.
* The more electrified track there is, the larger the area within battery range, particularly as battery technology improves.
* Battery range can be extended by prioritising electrification at route end-points, increasing the number of places that can be reached by battery power.
* Once battery trains are available, they can be used on lines due to be electrified as a transition. Once the line is electrified, the batteries can be removed, or the trains assigned elsewhere.
* As battery trains can be implemented faster than track electrification, the priority should be a rolling programme of replacing DMUs with BEMUs to run on unelectrified tracks within battery range. This should be combined and coordinated with a rolling programme of track electrification, which can replace the battery trains as it comes online.
* Because of the extra infrastructure needed to support hydrogen-powered trains, these will take longer to introduce; there needs to be a rolling plan for this for those routes beyond battery range.
* Proposals/plans for new/reopened lines should not be dependent on diesel, and should describe which decarbonised option they will use.
* All rolling plans need to be flexible so advances in technology can be incorporated; contracts with manufacturers need to reflect this. As with Network Rail's 5-year delivery plans, these should be reviewed at regular intervals.
* Given the long lead times for manufacturers to build/adapt trains, plus the time operators need to bring them into service, firm orders need to start being placed now, so manufacturers can plan production, finance can be arranged, and operators can plan implementation.
* Government needs to provide the overall strategy/framework for this to happen, and needs to decide how far this should be decentralised, to transport authorities in Scotland, Wales, North of England, Midlands, London, or anywhere else. This must be done quickly for any goals to be reached.
