---
title: Current diesels
name: diesels
edited: January 2022
---
### DMU

#### Sprinters Class 15n
* 150: Northern 78, GWR 20
* 153: Scot 5, Northern 15
* 155: Northern 7
* 156: Scot 43, Northern 51 + 20 from EMR in 2023
* 158: Scot 40, GWR 18, EMR 26, Northern 53, GWR 14
* 159: SWR 30

TfW has 36 150s, 26+6 153s, 24 158s, all to be replaced by 197s in 2023

#### Turbos
* 165: Chiltern 39, GWR 36
* 166: GWR 21

#### Turbostar
* 168: Chiltern 28
* 170: Crosscountry 28, EMR 13, Scot 35, Northern 16, WMT 16, TfW 11 to xfer to EMR in 2022/3
* 171: Southern 20
* 172: Chiltern 4, WMT 35

#### Coradias
* 175: TfW to be replaced by 197s in 2023
* 180: Grand Central 10, EMR replaced by 2023

#### Desiro
* 185: TPE 51

#### Civity
* 195: Northern 58
* 196: WM 26 by 2022
* 197: TfW 77 by 2023

### Diesel-electric
#### Voyagers
* 220: XC 34
* 221: Avanti 20 (replaced by electric and 805s by end 2022), XC 24
* 222: EMR 27 (replaced by 810s by end 2022)

#### HSTs
* 43: Scotrail 43, XC 13, GWR 35

#### Flirts
* 755: Anglia 38

#### Bi-modes
* 800: GWR, LNER
* 802: GWR, TPE, Hull Trains

#### 319 conversions
* 769: TfW 9, Northern 6, GWR 19

### DBEMU
#### Vivarail
* 220: TfW 5 from 2022

### Diesel loco-hauled
* 57: GWR 3
* 67: TfW: 7
* 68: Chiltern 6, TPE 14
* 73: Caledonian 6
