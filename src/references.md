---
title: References and Links
name: references
edited: December 2021
---
### Government/Parliament/Public bodies
* **UK government**
    * [Transport Decarbonisation Plan](https://www.gov.uk/government/publications/transport-decarbonisation-plan) for all transport modes. Commits to a large rail electrification programme and introduction of alternatives for non-electrified track; based on Network Rail's *TDNS*. More details are promised once Great British Railways is up and running.
    * The [Integrated Rail Plan for the North and Midlands](https://www.gov.uk/government/publications/integrated-rail-plan-for-the-north-and-the-midlands) is primarily about HS2, but does commit to electrification of the Midland Main Line to Sheffield, and the Trans Pennine Update (Manchester-York) by the early 2030s.
    * The [Union Connectivity Review](https://www.gov.uk/government/publications/union-connectivity-review-final-report) recommends setting up a strategic transport planning body, UKNET, to focus on major transport corridors - a similar concept to the major town links advocated in this website. Mentions electrification of several lines, including N Wales mainline.
* **Transport Scotland** has published its [rail decarbonisation action plan](https://www.transport.gov.scot/media/47906/rail-services-decarbonisation-action-plan.pdf) to decarbonise by 2035. See also their [prioritisation page](https://www.transport.gov.scot/public-transport/rail/prioritisation-of-rail-projects/).
* **Transport for the North** has given an [overview of their recommendations](https://transportforthenorth.com/press-release/gov-recommendations-northenpowerhouserail/) for improving the rail network across N England. This is primarily about improving travel times, and is not specifically about decarbonisation. It does however include electrification of Leeds-Hull and Sheffield-Hull, besides the main Liverpool-Manchester-Bradford-Leeds connection.
* [Commons **transport select committee** 2019 - Trains fit for the future - written evidence](https://www.parliament.uk/business/committees/committees-a-z/commons-select/transport-committee/inquiries/parliament-2017/trains-fit-for-future-17-19/publications/) - submissions from manufacturers and others concerned with railways. Of the manufacturers, Bombardier state their trains are modular and batteries can easily be added; recharge 7-10 minutes under catenaries; range up to 100km. Hitachi diesel bi-mode 5% greater weight than pure electric; design/build/deliver 2-3 years (their submission includes a map of which routes might be suitable for different technologies). See also [Government response](https://publications.parliament.uk/pa/cm5802/cmselect/cmtrans/249/24902.htm).
* The **Office of Rail and Road** (ORR) publishes [annual rail infrastructure and assets reports](https://dataportal.orr.gov.uk/statistics/infrastructure-and-emissions/rail-infrastructure-and-assets/), which include the amount of electrified track.

### Rail industry
* The [interim report](https://www.networkrail.co.uk/wp-content/uploads/2020/09/Traction-Decarbonisation-Network-Strategy-Interim-Programme-Business-Case.pdf) of the *Traction Decarbonisation Network Strategy*, led by **Network Rail** was published in 2020
* **Rail Industry Decarbonisation Taskforce**: [final report](https://www.rssb.co.uk/en/Research-and-Technology/Sustainability/Decarbonisation/Decarbonisation-our-final-report-to-the-Rail-Minister), July 2019; recommended amongst other things that each organisation within the rail industry should produce a long-term plan for decarbonisation.
* The **Rail Industry Association**'s [RailDecarb21](https://riagb.org.uk/RIA/Newsroom/Stories/RailDecarb21.aspx) campaign urges a rolling electrification programme plus orders for low-carbon trains. Its [*Electrification Cost Challenge*](https://riagb.org.uk/RIA/Newsroom/Publications%20Folder/Electrification_Cost_Challenge_Report.aspx) made recommendations for speeding up and reducing cost of track electrification.

### Campaign groups and press articles
* **Railfuture** gives their [current priorities](https://www.railfuture.org.uk/article1862-Electrification-mix) for electrification, describing implementation of battery trains as a 'quick win'. Their [freight page](https://railfuture.org.uk/Freight) also lists their priority lines as far as freight is concerned.
* [2021 Rail Engineer article](https://www.railengineer.co.uk/decarbonising-scotlands-railway/) on Scotland's decarbonisation plans
* [2021 Modern Railways article](https://www.modernrailways.com/article/freight-electrification-making-it-happen) gives priorities for freight
* as does [2021 Rail Freight Solutions article](https://www.railfreightsolutions.com/uploads/8/7/0/8/87088186/tdns_railfreight_solutions_priority_order_may_21.pdf)

### Non-decarbonisation rail data
* Phil Deaves' site:
    * [comprehensive list of ELRs](http://www.railwaycodes.org.uk/elrs/elr0.shtm), with all junctions, stations etc on each line, along with start and end mileage
    * [ELR/LOR xref](http://www.railwaycodes.org.uk/pride/elrmapping.shtm)
    * [list of stations](http://www.railwaycodes.org.uk/stations/station0.shtm), giving coordinates and operator, and linking with the appropriate ELR and mileage.
* **Network Rail** divides the network into [5 regions](https://www.networkrail.co.uk/running-the-railway/our-routes/) split into 14 sub-regions, which it rather confusingly calls *routes*
    * the [Sectional Appendix](https://www.networkrail.co.uk/industry-and-commercial/information-for-operators/national-electronic-sectional-appendix/) is the reference work of all the lines for which they are responsible. There's an online version (access available on request), or downloadable (large) pdfs of each route/region, which lists all lines by Line Of Route (LOR) code; details give ELR, max speeds, and whether electrified, and list mileage for every station, junction, loop, etc. Does not however give the timing points (TIPLOC) used in schedule data, and there doesn't appear to be a xref of which TIPLOCs are on which ELR/LOR, making it hard to match timetable data with line/route data.
    * the yearly [Network Statement](https://www.networkrail.co.uk/industry-and-commercial/information-for-operators/network-statement/) includes an overall national electrification map, but it's hard to discern details on this.
    * the [network specification](https://www.networkrail.co.uk/running-the-railway/long-term-planning/) for each of its routes/regions contains overview sketch maps of electrified/non-electrified lines along with max speeds, but these are mainly several years old, and not up-to-date.
* [OpenRailwayMap](https://www.openrailwaymap.org/) is a worldwide raster tile presentation of the railway network, updated daily. It uses OpenStreetMap data, but assumes the presence of certain tags, not consistently present for GB.
