/**
 * deno run --allow-read --allow-write processTracks.js
 *
 * The track file from Network Rail dates from 2012, and so is somewhat
 * out of date. It contains one record for each ELR, which does not correspond
 * to modern route tracks.
 * The meaning of several feature property names is not obvious.
 *
 * The electrified data also needs some adjustments, as some tracks are only
 * partially electrified; these are ignored.
 *
 * Most tracks are LineStrings but some are MultiLineStrings.
 * multis.json is a list of MultiLineString tracks, in 2 arrays 'splits' and
 * 'replaces'. The splits are split into LineStrings with 'ABC_0', 'ABC_1', ...
 * with 15nn fid. The others have simply been edited into LineString features
 * stored in a file <elr>.geojson for each 'replace'. These are output instead
 * of the supplied feature.
 *
 * So this script preprocesses the NR track file, and:
 * - adds new lines (in since2012/)
 * - updates electrified tracks using data from elec/elecrecs.json (those >40%)
 * - splits/replaces MultiLineStrings
 * - truncates all geo coords to 6 decimals (some 111mm of Latitude)
 * - only uses some feature properties, and renames where appropriate
 * - outputs to nrtracks.geojson and nrtracks.csv (properties without coords)
 * - simplifies the LineString coordinates (tolerance 0.0001) and outputs to
 *   tracks/tracks-simp.geojson, the file used by the process in tracks/
 *
 * Also outputs closed.json, an object list of ELRs with status CLOSED.
 * This can be moved to config/ for later use.
 */
import FeatureCollection from "../../tracks/FeatureCollection.js";

const dataDir = './datain/',
  newDir = dataDir+'/since2012/',
  replaceDir = dataDir+'/replacements/',
  outputDir = '../../tracks/',
  nrFile = dataDir+'/network4326.geojson',
  elecFile = 'elec/elecrecs.json',
  outputFilename = 'nrtracks.geojson',
  simpFile = outputDir+'tracks-simp.geojson',
  multiFile = './config/multis.json',
  adjustments = './elec/adjustments.json',
  closedFile = 'closed.json',
  csvFile = 'nrtracks.csv';

let tracks = getTrackData();
processElec();
processMultis();
outputFiles();

// original + since2012/
function getTrackData() {
  // NR tracks file
  let fileNames = [nrFile], allFeatures = [];

  // add lines not in original data (in since2012/)
  for (const dirEntry of Deno.readDirSync(newDir)) {
    fileNames = fileNames.concat(newDir + dirEntry.name);
  }

  // read in all track files
  fileNames.forEach(name => {
    const features = JSON.parse(Deno.readTextFileSync(name)).features;
    allFeatures = allFeatures.concat(features);
  });

  return allFeatures;
}

/*
 * takes NR elec data (consisting of different mileage for each line track),
 * calculates track mileage and works out what percentage of line track is electrified.
 * If no line track >40, treated as unelectrified (total no printed to console).
 * If line electrified, adds type (OLE or 3rd-rail) to feature.
 * Prints to console list of ELRs in elec data not in track file.
 */
function processElec() {
  const elecrecs = JSON.parse(Deno.readTextFileSync(elecFile));
  let notFound = [], none = 0;
  elecrecs.forEach((er) => {
    let elr = er[0];
    let track = tracks.find(t => t.properties.businessre == elr);
    if (!track) {
      notFound.push(elr);
    } else {
      let miles = Math.round((track.properties.highmeasur - track.properties.lowmeasure) * 10000) / 10000;
      let idParts = er[2];
      let opParts = [], highest = 0;
      idParts.forEach(part => {
        let idTotal = 0;
        part[1].forEach(pt => {
          let dist = Math.round((pt[1]-pt[0]) * 10000) / 10000;
          idTotal += dist;
        });
        let percent = Math.round((idTotal / miles) * 10000) / 100;
        if (percent > 40) {
          // skip if < 40% electrified
          opParts.push([part[0], Math.round(idTotal * 10000) / 10000, percent]);
          if (percent > highest) {
            highest = percent;
          }
        }
      });
      if (opParts.length > 0) {
        track.properties.elecType = er[1];
        track.properties.percent = highest;
      } else {
        none++;
      }
    }
  });

  console.log(`${none} ELRs with no section >40%`);
  console.log(`${notFound.length} elrs not found`, JSON.stringify(notFound));
}

/*
 * Splits MultiLineStrings into LineStrings.
 * Multis are defined in multis.json, as add, replace and elec.
 * With adds, new LineStrings are created for each part of the MultiLineString.
 * One of the new LineStrings is electrified, defined as elec; updated after creation.
 * There is a replacement LineString feature for each replace in replacements/.
 */
function processMultis() {
  //
  const multis = JSON.parse(Deno.readTextFileSync(multiFile));
  let splits = multis.splits;

  let fid = 1500;
  splits.forEach((m) => {
    let multiTrack = tracks.find(t => t.properties.businessre == m);
    let opTracks = [];
    multiTrack.geometry.coordinates.forEach((c, i) => {
      let props = {...multiTrack.properties}; // create new object to prevent closure
      props.ogc_fid = fid;
      fid++;
      // suffix with _
      props.businessre = props.businessre + '_' + i;
      // electrified split section
      if (multis.elec[props.businessre]) {
        props.elecType = multis.elec[props.businessre];
      }
      opTracks.push({
        type: multiTrack.type,
        properties: props,
        geometry: { type: "LineString", coordinates: c}
      });
    });
    // add to output tracks
    tracks = tracks.concat(opTracks);
  });

  let replaces = multis.replaces;
  replaces.forEach(m => {
    let replacement = JSON.parse(Deno.readTextFileSync(`${replaceDir}/${m}.geojson`));
    tracks.push(replacement);
  })
}

/*
 * Outputs files as both GeoJSON FeatureCollection and CSV (without coords).
 * Ignores original MultiLineStrings.
 * Renames feature properties.
 * Rounds coords to 6 dec places.
 * Uses adjustments.json to update elecType as appropriate for split track sections,
 * and those currently being electrified (WIP).
 * Also outputs a JSON file for the closed/private/siding tracks.
 */
function outputFiles() {
  // elec adjustments
  const adds = JSON.parse(Deno.readTextFileSync(adjustments)).add;

  // output feature collection
  let fc = new FeatureCollection('tracks');
  let closed = {}, closedCnt = 0, csvop = [];

  tracks.forEach(t => {
    // ignore MultiLineStrings
    if (t.geometry.type != 'LineString') {
      return;
    }

    // only use useful properties
    let props = {};
    props.name = t.properties.businessre; // ELR
    props.startMile = t.properties.lowmeasure;
    props.endMile = t.properties.highmeasur;
    props.elecType = t.properties.elecType || '-';
    if (adds[props.name]) {
      props.elecType = adds[props.name];
    }
    if (t.properties.percent) {
      props.percent = t.properties.percent;
    }

    let outFeature = {
      "type": "Feature",
      "id": t.properties.ogc_fid,
      "geometry": {"type": "LineString"},
      properties: props
    };
    let geom = t.geometry;
    let coords = [];
    // round coords to 6 dec places
    geom.coordinates.forEach((c) => {
      c[0] = Math.round(c[0] * 1000000) / 1000000;
      c[1] = Math.round(c[1] * 1000000) / 1000000;
      coords.push(c);
    });
    outFeature.geometry.coordinates = coords;
    fc.addFeature(outFeature);

    // include original status and type in CSV file
    csvop.push([props.name, props.startMile, props.endMile, outFeature.id, props.elecType, props.percent, t.properties.status, t.properties.type]);

    // if closed or private/siding, add to closed object
    if (t.properties.status == 'CLOSED') {
      closed[props.name] = t.properties.status;
      closedCnt++;
    }
    if (['PRIVATE','SIDING'].includes(t.properties.type)) {
      closed[props.name] = t.properties.type;
      closedCnt++;
    }
  });

  // simplify track coordinates
  let simpFC = new FeatureCollection('simp');
  simpFC.simplifyAll(fc.getFeatures());
  simpFC.writeFile(simpFile); // to tracks_simp.geojson
  fc.writeFile(outputFilename);

  csvop.sort();
  let csvout = `elr,low,high,id,elecType,percent,status,type
${csvop.join('\n')}`;
  Deno.writeTextFile(csvFile, csvout)
  .then(() => {
    console.log(`${csvop.length} csv recs saved`);
  })
  .catch(err => {
    console.log(err);
  });

  const ordered = {};
  Object.keys(closed).sort().forEach(function(key) {
    ordered[key] = closed[key];
  });
  Deno.writeTextFile(closedFile, JSON.stringify(ordered))
  .then(() => {
    console.log(`${closedCnt} saved in ${closedFile}`);
  })
  .catch(err => {
    console.log(err);
  });
}
