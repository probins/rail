const stations = require('./files/stations27700.json').features,
  fs = require('fs');

let sortBy = [{
  prop:'primary_el',
  direction: 1
},{
  prop:'cl_mileage',
  direction: 1
}];

stations.sort(function(a,b){
  let i = 0, result = 0;
  while(i < sortBy.length && result === 0) {
    result = sortBy[i].direction*(a.properties[ sortBy[i].prop ].toString() < b.properties[ sortBy[i].prop ].toString() ? -1 : (a.properties[ sortBy[i].prop ].toString() > b.properties[ sortBy[i].prop ].toString() ? 1 : 0));
    i++;
  }
  return result;
});

fs.writeFile('files/sortedstations.json', JSON.stringify(stations), function(err) {
  if(err) {
    console.log(err);
  } else {
    console.log("sortedstations.json saved");
  }
});
