---
title: Priorities
name: priorities
edited: December 2021
---
### Government policy
As stated in the status page, a detailed plan from government is presumably awaiting the formation of GBR and the establishment of UKNET, likely to be 2024 at the earliest.

The *Rail Industry Decarbonisation Taskforce* (RIDT) report stated "the government should set out clear, consistent and enabling policies." Siemens, in their submission to the Commons Select Committee, stated that "The biggest challenge to employing alternatively fuelled rolling stock on the rail network is the lack of an overall strategy for the railways."

The *TDNS* aims to "identify preferred combinations of electrification, hydrogen and battery traction options to achieve the most cost-effective low carbon outcome. This should consider not only the preferred long-term solution but also the most effective transitional arrangements.” The latter point is important: simply listing which tracks should be electrified or which routes should be run by battery-powered trains is not sufficient.

In summary, there needs to be an overall priority list, with a detailed plan of which manufacturers are supplying what when to whom. Such plans rarely go exactly to schedule, so there needs to be flexibility by all parties as well.

### Objectives
1. prioritise emission reduction in larger cities/towns first
2. as electrifying the trains can be done much faster than electrifying the tracks, with little disruption of current service, this should be started asap
3. electrify most important/most-used tracks first:
    * those between larger cities/towns (main lines)
    * commuter lines into larger cities/towns (many of these are on main lines)
    * most-used freight lines, including those from/to major ports

Note that electrifying the track not only improves those lines, but also brings more lines within battery range.

### Definitions
If the objective is to electrify the most important and most used lines first, then we have to define what that means. The *UCR* defines corridors, including both road and rail. For rail, although there is some use in Britain of the term 'main line', as in E and W Coast Main Line, it is not consistently applied. This site uses the term to distinguish the long-distance routes with the highest potential usage: for passenger trains, those between the most heavily populated towns; and for freight, those linking the main ports/handling centres with those main lines.

#### Larger towns
There are many definitions of 'town/city', but this site uses [The Geographist's list](https://www.thegeographist.com/uk-cities-population-100/) (based on urban sub-divisions). The *UCR* uses 500,000 as its criterion, but this excludes many densely populated areas. It also includes growing towns in S England, but this also ignores the many densely populated areas elsewhere, and runs contrary to the levelling up agenda. So this site takes those with a population over 200,000 to be 'large'. The Geographist list does not treat Teesside as one urban area; as this is also a major port, this can be added, and Sunderland included in the connection to Newcastle. Norwich is just below 200,000, but is a regional capital, and included here as it is in the *UCR* plan (the lines linking Norwich with the E Midlands are not really main lines, but are important for freight from Felixstowe too, so are considered a priority). Many other population centres are on the lines linking these towns, and of course many of these lines are already electrified.

It's noticeable that as of 2021 6 of the cities over 250,000 (Bristol, Sheffield, Leicester, Nottingham, Hull, Derby) have no electrification. All except Hull were originally scheduled for electrification by 2020; 4 of those (Sheffield, Leicester, Nottingham, Derby) are on the Midland mainline, now not due until the early 2030s.

#### Major ports
Several of the ports listed in the *UCR* - such as Liverpool, Southampton, Hull, Plymouth - are already included here because of size. Holyhead, along with connections to Crewe and Warrington, can be added, as can the Milford Haven line as far W as Swansea.

#### Freight
Although these pages are mainly about passenger transport, freight to a large extent uses the same tracks. Because electric locomotives can move faster than diesel ones, switching to electric wherever possible will also reduce conflicts with passenger services. Some of the freight lines linking ports and other major handling centres with the main lines are short fill-ins to already electrified track, and would not cost much to implement. Others are longer, but electrifying them would also benefit any passenger services on these lines.

If we combine the UCR's list of ports, the suggestions made by Railfuture and in the 2 articles listed in the references page, then we get the following additions to the passenger 'main lines':

##### Freight-only fill-ins
* ports: London Gateway; Liverpool; Teesport
* London: Acton to Mitre Bridge Jcn; Carlton Rd-Junction Rd; Dudding Hill line (also proposed as passenger service)
* Frome-Mendip quarries

Probably a total of 15-20 route km.

##### Longer lines not a priority for passenger services
* Felixstowe-Ipswich-Ely (shown as priority on map) + Manton-Corby (20 route km)
* East-West Rail (some 80 route km Oxford-Bedford) (once complete to Cambridge, Norwich can link to it)
* Immingham-Doncaster (some 65 route km)
* Hare Park-Wakefield-Leeds Stourton (some 35 route km)
* S Kirkby-Colton Jcn (some 30 route km)
* Milford Haven-Swansea/Cardiff (some 120 route km)

A total of some 350 route km

### Batteries
The current map makes clear that there are large numbers of lines which are currently in battery range, and quite a few more which could be within range if there's recharging at the terminus. So the priorities here seem to be:
1. aim to reduce/eliminate emissions in the most populated areas first
2. replace diesel with electro-battery trains for those routes which run between electrified lines or are branch lines within range
3. prioritise partial electrification at termini and other strategic points to maximise the usage of battery trains. It may also be possible to redesign routes to optimise recharging.
4. have flexible arrangements/contracts with manufacturers so that:
    * batteries can be replaced as and when better versions are available
    * diesel bi-modes can be replaced with batteries and/or hydrogen as quickly as possible
    * batteries can be removed as and when tracks are electrified

It's hard to be too sure what battery ranges will be in 5 or 10 years time, but current electric cars can manage 300-500km or more, something which is expected to improve in the coming years, especially if solid-state ones make it to mass production. The recent 3-year battery trial in Germany found that the Stadler units can reliably manage 185km without a recharge, and have managed 224km. Manufacturers are planning electric HGVs with 500km range by mid-decade, so it's surely not unreasonable to expect train batteries to manage 250km by 2025, and 500km by 2030. This means that all routes in Britain would be reachable with battery power, especially with intelligent use of station recharging. Shorter routes can use a smaller battery, thereby reducing the weight. The maps use these assumptions.

### Hydrogen
At the moment, those lines outside battery range, for example, in N Scotland, most of Wales, SW England, will have to use trains with a hydrogen tank. But even here, it may be possible to reduce the size needed by a program of partial electrification, along with use of hydrogen-electric bi-modes. And if batteries develop as anticipated, by 2030 hydrogen may well no longer be needed.

### Other proposed new/reopened lines
Proposals for new/reopened lines should commit to a decarbonised option; as the *TDNS* says, it makes no sense for them to plan on using diesel.
