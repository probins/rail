---
title: Home page
name: index
edited: December 2021
---
**Phasing out diesel on rail passenger services in Britain**

The UK government is committed to a net zero economy by 2050. The rail industry has over several years now put forward proposals for achieving this for rail transport, which the government has broadly agreed to. But to date there are little to no detailed plans from government on the way forward.

This site attempts to:
* monitor the fast-moving technologies and where they're heading
* describe the status with competing transport modes
* monitor the progress in some continental countries
* discuss the current status in Britain, the assumptions and suggested priorities

Map pages illustrate how decarbonisation could be achieved, and further pages describe the maps and how to use them, plus how to help with updating both textual and map pages.

Finally, a references page gives links to documents mentioned on other pages, plus others for further relevant information.

Use the menu above to navigate to these pages.
