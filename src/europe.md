---
title: Some implementations in Europe
name: europe
edited: January 2022
---
Most European countries have a higher percentage of electrified line than Britain, but still face similar decarbonisation issues. Switzerland is the only country with fully electrified track. Most other countries have track electrification projects, but this is very costly and would take decades to complete. So at the same time they are planning, with varying degrees of urgency, to replace diesel trains with electric ones. Most of the train suppliers in Britain also serve continental countries, and all are likewise overhauling their train models to do this.

### Austria
Currently, some 3,500 km of the 4,865 route km (some 73%) of Austria's rail network is electrified, and the 2017 [decarbonisation plan (in German)](https://konzern.oebb.at/dam/jcr:01321270-8905-4b63-a1e5-071ac09f8699/Nachhaltigkeitsbericht-2017-18.pdf), aims for 85% by 2030 and 89% by 2035. At the same time, battery and hydrogen alternatives will be introduced on unelectrified lines, so that diesel trains can be withdrawn from the entire network by 2030. The [latest investment plan](https://www.railwaygazette.com/infrastructure/full-electrification-in-record-infrastructure-spend/57601.article) plans 500km of track to be electrified by 2030, with investments in electricity from hydro and solar.

The battery-driven [**Siemens** Cityjet Desiro ML Eco](https://www.oebb.at/de/neuigkeiten/cityjet-eco) has been on trial in Lower Austria and Styria since 2019; range 80km, top speed 120kph on battery (further details [here](https://www.urban-transport-magazine.com/en/siemens-obb-cityjet-eco-achieves-approval-for-passenger-operation/) and [here](https://assets.new.siemens.com/siemens/assets/api/uuid:b26911b1-2b0e-48b4-b593-81adbf032d75/db-desiro-ml-oebb-cityjet-eco-e.pdf)).

As for hydrogen, the Coradia iLint (see Germany below) is being tested on [more demanding lines around Wiener Neustadt](https://www.oebb.at/en/neuigkeiten/wasserstoffzug) in autumn 2020.

The narrow-gauge Zillertalbahn has [ordered 5 units from **Stadler**](https://www.railjournal.com/fleet/zillertalbahn-hydrogen-train-design-revealed/) to replace its aging diesel locomotives, due to be operational by end 2022. The hydrogen will be locally produced, using the Zillertal's existing hydro-electric plant (some 30% of Austria's total capacity).

(Stadler is also building [FLIRT H2](https://www.railway-technology.com/news/stadler-deliver-hydrogen-powered-train-sbcta/) for San Berdardino in California, max speed 130kph.)

### Denmark
Manufacturers are being invited to test [battery-powered trains](https://www.railway-technology.com/news/denmark-trial-battery-operated-trains/) on 2 lines in Denmark in 2020-1, with a view to them entering service in 2025.

### France
The SNCF have long run electro-diesel bi-modes in their regional trains, but are now committing to [replacing all diesel power](https://www.sncf.com/en/passenger-offer/travel-by-train/ter/planeter-more-ters-less-co2) by 2035 ([PlaneTER (in French)](http://medias.sncf.com/sncfcom/newsroom/pdf/DP_NR_PLANETER_23-09-2020.pdf)).
* testing of [**Alstom** Régiolis with additional batteries](https://www.sncf.com/fr/innovation-developpement/innovation-recherche/ter-hybrides-bientot-dans-vos-gares) continues, with a view to full implementation in 2023
* 5 existing [**Bombardier** bi-modes](https://www.urban-transport-magazine.com/en/sncf-and-bombardier-convert-diesel-to-battery-trains/) are being converted to battery-electric, with a range of 80km; rollout in 2023
* a first order of 11 hydrogen-electric [**Alstom**'s Coradia Polyvalent](https://www.alstom.com/press-releases-news/2021/4/first-order-hydrogen-trains-france-historic-step-towards-sustainable) has been placed, with roll-out from 2025.

### Germany
As of 2020, 20,365 of 33,286km (61%) of Germany's track was electrified, and the plan is for this to rise to [70% by 2025](https://www.allianz-pro-schiene.de/themen/infrastruktur/daten-fakten/). This will still leave some 10,000km of unelectrified track, but the new government is keen to decrease this.

* **Stadler** Flirt Akkus. Range of up to 150km. Batteries from Intilion.
    * Schleswig-Holstein have ordered [55](https://www.stadlerrail.com/en/media/article/stadler-supplies-55-battery-operated-flirt-trains-for-theschleswig-holstein-local-transport-association/522/): five pre-series trains from November 2022, with the remainder to enter service from May 2023 to mid-2024. New charging points will also be installed where overhead lines are not available.
    * In [Rheinland Pfalz](https://www.railwaygazette.com/passenger/battery-emus-ordered-to-replace-diesel-on-partially-electrified-network/60416.article) 44 deliveries between December 2025 and end 2026. €50m will be spent to install overhead electric ‘islands’
* Rhein Ruhr (VRR) and Westfalen-Lippe have ordered 63 [CAF Civity](https://www.caf.net/upload/prensa/notas/docs/Comunicado-proyecto-VRR-NIEDERRHEIN-.pdf), delivery 2025-8
* [**Siemens** Mireo Plus B](https://press.siemens.com/global/en/feature/battery-powered-mireo-plus-b-decarbonises-europes-railways) trains, with a range of >90km and a [top speed](https://assets.new.siemens.com/siemens/assets/api/uuid:4c3fe5f8-a500-44d9-ad0c-7f3fe792c71f/mors-b10040-00dbmireoplusplattformenus-72.pdf) of 160kph
    * 20 have been ordered for part of the Ortenau network in Baden-Württemberg (N part of Black Forest), 80km range as 2-car, 120km as 3-car, delivery by June 2023. A further three will run on the new Hermann-Hesse-Bahn between Renningen and Calw from 2023
    * The Niederbarnimer Eisenbahn has also [ordered](https://www.neb.de/unternehmen/aktuelles/details/batterieelektrische-zuege-fuer-das-netz-ostbrandenburg/) 31 Mireo Plus Bs for the unelectrified lines in their network to the N and E of Berlin, delivery from 2024.
* Two trials of the hydrogen version of this, the [Mireo Plus H](https://www.mobility.siemens.com/global/en/portfolio/rail/stories/mireo-plus-h-for-a-cleaner-emissions-free-operation.html), with a range of up to 600 km as a two-part train, and from 800 to 1,000 km as a three-part train, are scheduled:
    * a [30-month one](https://press.siemens.com/global/en/feature/siemens-mobility-develops-hydrogen-train-climate-neutral-rail-transport-bavaria) around Augsburg from mid-2023
    * a [year-long one](https://railcolornews.com/2020/11/23/de-h2goesrail-siemens-and-db-enter-the-hydrogen-age-with-the-mireo-plus-h/) in 2024, based in Tubingen, along with production of green hydrogen.
* The 12-month trial in Alb-Bodensee (S part of Black Forest) of **Bombardier**'s [Talent 3](https://rail.bombardier.com/en/solutions-and-technologies/mainline/regional.html), a collaboration with the Berlin Technical University with partial funding by the Federal Government, was originally planned for 2019, but delayed. Bombardier was acquired by **Alstom** in 2021, with the Talent 3 programme transferred to CAF. But Alstom is [completing these trials](https://www.alstom.com/press-releases-news/2022/1/alstom-and-deutsche-bahn-test-first-battery-train-passenger-operation) in 2022. They are fitting the technology to the Coradia Continental, and Verkehrsverbund Mittelsachsen have ordered [11 units](https://www.alstom.com/press-releases-news/2020/2/alstom-signs-first-contract-battery-electric-regional-trains-germany) for use on the Chemnitz-Leipzig line, delivery from 2023, up to 120km range, charging time 30 mins.
* **Alstom**'s hydrogen-powered [Coradia iLint](https://www.alstom.com/our-solutions/rolling-stock/coradia-ilint-worlds-1st-hydrogen-powered-train) has acquired much publicity, being operational on the Elbe-Weser network (west of Hamburg) since 2018, with a top speed of 140kph. 22 further units are ordered for those lines, with delivery from 2022, and Alstom has a further [order of 27](https://www.alstom.com/press-releases-news/2019/5/rmvs-subsidiary-fahma-orders-worlds-largest-fleet-fuel-cell-trains) for the Taunus network (north-west of Frankfurt), also with delivery from 2022. For the latter, the plan is to use by-products from the chemical industry in Hoechst. The hydrogen for Elbe-Weser is provided by [Linde](https://blog.ballard.com/hydrogen-fuel-distribution), and the plan is for it to eventually be produced from local wind turbines.

### Hungary
MAV-Start have issued a [call for tender](https://www.railwaygazette.com/mav-start-seeks-up-to-50-battery-electric-trainsets/57212.article) for battery-powered trains in Hungary; range up to 80km at a speed of 100 km/h.

### Ireland
Iarnród Éireann has [ordered](https://www.rte.ie/news/dublin/2021/1213/1266432-dart-carriages/) 13 battery-electric Alstom trains for use on its Dublin-Drogheda service from 2025; battery range over 80km, with recharge time under 20 minutes.

### Italy
* Ferrovie Nord Milano has [ordered](https://www.alstom.com/press-releases-news/2020/11/alstom-supply-italys-first-hydrogen-trains) 6 hydrogen-powered **Alstom** Coradia Streams, with an option for 8 more, delivery starting in 2022.
* FS and SNAM are also [cooperating](https://www.railwaygazette.com/italian-hydrogen-train-co-operation-agreement/57647.article) with Alstom to evaluate hydrogen-powered trains, with the aim of rapidly converting trains currently powered by diesel.

### Netherlands
* following [successful testing](https://www.alstom.com/sites/alstom.com/files/2020/09/30/Rapport%20Waterstoftrein%20Groningen_EN.pdf), Groningen is [planning](https://www.railtech.com/rolling-stock/2020/10/01/hydrogen-trains-will-replace-diesel-units-in-groningen/) to replace diesel with the **Alstom** iLint, delivery 2021-4.
* Friesland originally hoped to fully batterify their trains by 2025, but have now [postponed this to 2035](https://www.ovpro.nl/trein/2019/11/15/geen-batterijtreinen-in-friesland-in-2025/), or earlier if possible. Concession holder Arriva will however from 2021 be using 18 [**Stadler** Winks](https://www.stadlerrail.com/en/products/detail-all/wink/198/), running partially on Hydrotreated Vegetable Oil (HVO), a [biodiesel powering batteries](https://www.railvolution.net/news/winks-in-regular-service-and-a-couple-of-questions-over-the-future-operation).

### Portugal
* a main thrust of the [PNI2030 National Investment Plan](https://www.portugal.gov.pt/download-ficheiros/ficheiro.aspx?v=%3d%3dBQAAAB%2bLCAAAAAAABAAzNDCztAAAjDIorAUAAAA%3d) is a programme of electrification (Annex A.1, F5)
* CP have [ordered](https://www.railwaygazette.com/traction-and-rolling-stock/cp-orders-1-668-mm-gauge-electric-and-bi-mode-multiple-units/57629.article) **Stadler** Flirt bi-modes, initially running on diesel, but with the option of later converting to battery.

### Spain
* [**CAF**](https://www.caf.net/en/sala-prensa/nota-prensa-detalle.php?e=316) is leading the EU-funded FCH2RAIL project, developing a hydrogen-powered version of their Civia trains, for use from 2024 in Spain, Portugal and another as yet unnamed country
* **Talgo** has announced its [Vittal-One](https://www.talgo.com/-/talgo-s-hydrogen-train-will-be-ready-in-2023) hydrogen-powered train, available from 2023.
