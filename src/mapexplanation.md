---
title: Map explanation
name: mapexplanation
edited: December 2021
---
On startup the map shows all passenger railway lines in GB, both those in operation and those being (re)opened. Those in the original NR data used purely for freight or by metro/tram lines or marked as closed, private or siding, can be shown by clicking on the 'hamburger' menu, selecting 'Layers', and then ticking the box for the appropriate layer(s). The layers can be switched on and off in this way.

The 'main' lines defined in the Priorities page are shown thicker.

The electrified lines - OLE and 3rd-rail - and those currently being electrified are shown in various shades of green. The unelectrified lines are shown in brown, and those being (re)opened in red.

On start-up, the current electrification status is shown. There are additional layers for the status for each 5 year period listed in the Plans page: 2025/2030/2035. These can be seen by clicking on the current layer and then on the other layer.

Unelectrified lines within there-and-back range of electrified track (i.e. total battery range if electrified at both ends, or half that or less if electrified at one end only) are shown with dotted lines. Those within 1-way range, i.e. those which would be usable by battery trains if there were recharging at the non-electrified end, have short-dashed lines. Long dashed lines are for those with no electrification at either end, but which would be within range if there were recharging facilities at one or both ends. This is not too exact for those which are not simple there-and-back branch lines, as different routes can have different end-points and use only sections of a given line. However, it gives a rough guide to how many routes can be electrified with recharging facilities at strategic points. The lines which are scheduled for track electrification are not included in this battery calculation, as the investment needed for the battery trains would not be justified for the comparatively short time they would be needed.

The battery range can be changed from the default 100km to 200/300/400km by clicking on both layers as with electrification.

The data is based on the [Engineer's Line Reference (ELR)](http://www.railwaycodes.org.uk/elrs/elr0.shtm), the system which was the main reference within the rail industry (now largely replaced in NR's reference works by the Line of Route (LOR) code). A lot of ELRs are based on the original railway companies' lines, which no longer correspond to modern train or operator routes. This means many of the ELR tracks have to be split to reflect those which are now only partly used, partly electrified, or partly freight only.

The ELRS for the main and unelectrified lines are grouped together, and given a more meaningful title. Hovering over a line section will display this title, and the km of that section. All the sections of a group will display the same title, so the individual kms for all the sections will have to be added together to get the total for the whole group. The km is as measured on the map, and will not correspond exactly to the official NR mileage.

To keep the size of the data transferred to the browser manageable, the lines displayed have been simplified (using the [Ramer–Douglas–Peucker algorithm](https://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm)); this means that when you zoom right in, the tracks no longer correspond exactly to the line shown on the base map. This is purely to speed display of the map, and does not reflect any problem with the original data.

Clicking on a line displays its properties, including the ELR: 'startMile' and 'endMile' are the start and end mileage of the line, as supplied by NR.

It's hard to keep track of which proposals for line reopening are at which stage of the convoluted process, so this site uses [Railfuture's helpful list](https://railfuture.org.uk/Restoring+your+railway).

The rail data are by default displayed over the Ordnance Survey 'Light' base map. By clicking the 'hamburger' menu top-left and selecting 'Layers', you can change this to the standard OpenStreetMap render, which can zoom in to provide further detail of the surroundings. At a very detailed zoom level, the OSM map should show the different tracks of the lines; this site's line data does not show this kind of detail.

As circumstances change, the map can be changed to reflect such. See the Updating page on how to do this.

#### Compiling and maintaining the data
The data, scripts, and source for these webpages are stored in [GitLab](https://gitlab.com/probins/rail), and deployed to Cloudflare Pages. See the Readme in the repository for details of how this hangs together, and see the Updating page for details on how to make small changes. Contributions are more than welcome!
