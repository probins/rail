### Map creation
#### Original data
There are several sources for open railway line data, none of which is ideal. Network Rail published [track and station data](https://data.gov.uk/dataset/c0bedef1-8237-4d14-8bb0-30cec2cd4c9e/network-rail-railway-network-inspire-data) in 2012 as part of the EU's INSPIRE directive. This has not been maintained, but the track data is used as the basis for this map. Fortunately, railway tracks do not change very much, and the only new line since 2012 is the Scottish Borders line to Galashiels, plus the Barking Riverside link (under construction), and a few chords and flyovers. The track for the Borders line, Riverside, and Ordsall Chord have been taken from OpenStreetMap; other flyovers such as at Stafford and Nuneaton are not included (they don't significantly change the overall picture). There were also a couple of lines missing from the data, the East London Line and the line to Heathrow Terminal 5 (the data were probably compiled whilst these were being built/restructured). These have all been added. In principle, track data is also freely available from OpenStreetMap, but this data is not really structured in a way suitable for this application (see `osm/notes` for more info).

The 2012 data provides one track for each [Engineer's Line Reference (ELR)](http://www.railwaycodes.org.uk/elrs/elr0.shtm), the system which was the main reference within the rail industry, but which has been replaced in NR's reference works by the Line of Route (LOR) code. ELRs are largely based on the original railway companies' lines, which no longer corresponds to modern train or operator routes. This means many of the ELR tracks have to be split to reflect those which are now only partly used, or partly freight only.

Electrification data were obtained from Network Rail with a [Freedom of Information request](https://www.whatdotheyknow.com/request/electrification_data#incoming-1418317) in 2019. These data are however very low-level (36MB), and had to be consolidated for each ELR. As some ELRs have only been partially electrified, these too have to be split accordingly. As electrification is ongoing, the data has to be updated as appropriate.  

#### Creating the original map
As stated above, the original NR data are out-of-date and do not distinguish freight/passenger. So to get to the current status:
- add new lines
- separate closed/private lines
- modify and add electrification
- separate freight only lines
- lines can be partly frt, and partly electrified, so may need splitting
- which lines within battery range
- technical: truncate coords; remove MultiLineStrings; rename properties
For browser:
- simplify

The files and scripts used to create the first map are in `nr/tracks/`. The track data is in `datain/`: one file for the original NR data, plus the additional ones in `since2012/`. Some lines were MultiLineStrings; as this complicates the code, these were either split into LineStrings, or replaced by a manually created LineString (in `replacements/`); see `config/multis.json`.

The electrification data, in `elec/`, were consolidated into `elecrecs.json` using `elec_merge.js`. ELRs to add/remove from this data are in `config/amendelecs.json`. `processTracks.js` then took this data and added it to `nrtracks.geojson` used as the basis data. It also truncated all geo coordinates to 6 decimal places (around 11mm of latitude, ample for maps at this scale). The script then simplifies the geo coords (using the [Ramer–Douglas–Peucker algorithm](https://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm)), and writes to `tracks-simp.geojson`. ('Simplifying' means smoothing the lines to the minimum needed to display at a certain scale, thereby reducing the file size sent to the browser.)

See `processTracks.js` source for more info, and `notes` contains some further details on various attempts to squeeze usable info out of open data.
