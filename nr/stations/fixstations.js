/**
 * node fixstations.js
 * read stations4326.geojson
 * remove float junk, truncate decimals on lat/lon, add tiploc from tiplocs.json
 * output fixedstations.csv, sorted by ELR/mileage, and fixedstations.geojson
*/

const fs = require('fs'),
    os = require('os');

const ipgeo = './stations4326.geojson',
    opcsv = './stations.csv',
    opgeo = './stations.geojson';

const tiplocs = require('../tiplocs.json');

// read stations file
const stations = JSON.parse(fs.readFileSync(ipgeo, 'utf-8')).features;

let csv = [], lineCnt = 0,
    header = 'primary_el,cl_mileage,ogc_fid,name,type,fo_code,fo_name,stn_code,latitude,longitude,gis_eastin,gis_northi,tiploc',
    fc = {"type": "FeatureCollection", "features": []};

stations.forEach((s) => {
  let props = s.properties;
  // look up tiploc
  let objIndex = tiplocs.findIndex(obj => obj.crs === props.stn_code);
  if (objIndex == -1) {
    console.log(`crs ${props.stn_code} ${props.name} not found`);
  } else {
    props.tiploc = tiplocs[objIndex].tiploc;
  }

  props.gis_eastin = Math.round(props.gis_eastin);
  props.gis_northi = Math.round(props.gis_northi);

  let lat = Math.round(s.geometry.coordinates[0] * 1000000) / 1000000,
      lon = Math.round(s.geometry.coordinates[1] * 1000000) / 1000000;

  // add to featurecollection
  fc.features.push({
    "type": "Feature",
    "geometry": {
      "type": "Point",
      "coordinates":  [lat, lon]
    },
    "properties": props
  });

  let fields = [
    props.primary_el, props.cl_mileage, props.ogc_fid, props.name, props.type,
    props.fo_code, props.fo_name, props.stn_code, lat, lon,
    props.gis_eastin, props.gis_northi, props.tiploc
  ];

  // add to csv
  csv.push(fields);

  lineCnt++;
});

  // save header record
  // if (lineCnt == 0) {
  //   header = line + ',crs';
  // } else {
  //   // hack to fix embedded ,
  //   if (line.includes('Terminals 1,2')) {
  //     line = line.replace('Terminals 1,2', 'Terminals 1 2');
  //   }
  //   let fields = line.split(',');
  //   // truncate lat/lon to 6 decs
  //   fields[8] = Math.round(fields[8] * 1000000) / 1000000;
  //   fields[9] = Math.round(fields[9] * 1000000) / 1000000;
  //   // remove decs from east/north
  //   fields[10] = Math.round(fields[10]);
  //   fields[11] = Math.round(fields[11]);
  //   // look up tiploc
  //   let objIndex = tiplocs.findIndex(obj => obj.crs === fields[7]);
  //   if (objIndex == -1) {
  //     console.log(`crs ${fields[7]} ${fields[3]} not found`);
  //   } else {
  //     fields.push(tiplocs[objIndex].crs);
  //   }
  //   json.push(fields);
  //   fc.features.push({
  //     "type": "Feature",
  //     "geometry": {
  //       "type": "Point",
  //       "coordinates":  [fields[9],fields[8]]
  //     },
  //     "properties": {
  //       "primary_el":fields[0],
  //       "cl_mileage": fields[1],
  //       "ogc_fid": fields[2],
  //       "name":fields[3],
  //       "type":fields[4],
  //       "fo_code":fields[5],
  //       "fo_name":fields[6],
  //       "stn_code":fields[7],
  //       "gis_eastin":fields[10],
  //       "gis_northi":fields[11],
  //       tiploc: fields[12]
  //     }
  //   });
  // }
  // lineCnt++;

csv.sort();

let out = `${header}\n${csv.join(os.EOL)}`;
// write fixed CSV file
fs.writeFile(opcsv, out, function(err) {
  if(err) {
    console.log(err);
  } else {
    console.log(`stations.csv saved with ${lineCnt} records`);
  }
});
// write geojson file
fs.writeFile(opgeo, JSON.stringify(fc), function(err) {
  if(err) {
    console.log(err);
  } else {
    console.log(`stations.geojson saved`);
  }
});
