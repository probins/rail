---
title: Electrification plans
name: plans
edited: December 2021
---
### Scotland
Scotland is the only part of GB with a detailed plan:

#### To 2025
The priorities are for the remaining central lowlands routes to be electrified. E Kilbride and Kilmarnock as far as Barrhead are funded. Next are the Forth bridge line to Dalmeny, which together with the reopening Levenmouth line would put battery trains in range of Edinburgh. Similarly, the Borders line will be partially electrified to enable battery trains to operate on the whole line. That leaves the Maryhill line, which could also run on batteries.

#### To 2030
Fife, plus Stirling-Dundee, would be next; total route km some 230, say 500 stk.

#### To 2035
* Dundee-Aberdeen, some 120 route km
* Kilmarnock-Carlisle, some 140

### Wales
#### To 2025
S Wales Metro, some 100 route km, is scheduled for completion by 2023. This would leave time for the team to move on to other lines by 2025, but there is no current budget for this.

#### Remaining priorities
* Cardiff-Swansea (around 80 route km, though there is also a plan to create a new, more direct route east of Swansea, and it would make sense to do this at the same time)
* N Wales (some 140 route km, plus 60 for connections to Crewe and Warrington, where there should also be a connection to the HS2 line to Manchester)

Say 550stk, which, given funding, one team could finish in 6 years, so could be finished by 2030.

Priorities in England for Wales would be Cardiff-Bristol-Southampton and Cardiff-Gloucester-Birmingham.

### England
#### To 2025
The only projects scheduled for this period are:

##### Midlands
* MML to Market Harborough (14 route km). Once complete, the team can presumably continue on further towards Leicester.

##### North
* The York-Church Fenton section of the [Transpennine Upgrade](https://www.networkrail.co.uk/running-the-railway/railway-upgrade-plan/key-projects/transpennine-route-upgrade/) (9 route km, scheduled for completion October 2022); plus preparatory work on Manchester-Stalybridge, and possibly Huddersfield-Dewsbury
* Bolton-Wigan (12 route km)

##### South
* The 4.5km new line to [Barking Riverside](https://tfl.gov.uk/travel-information/improvements-and-projects/barking-riverside-extension), scheduled for Autumn 2022, is electrified.

#### To 2030
There is a commitment for 2 teams:

##### Midlands
* Mkt Harborough-Derby-Sheffield plus branch to Nottingham and loop via Rotherham is some 150 route km; Trent Jcn through Toton to Clays Cross some 40 route km. Although the IPR has this complete by early 2030s, the original plan to electrify Kettering to Sheffield and Nottingham was to take 4 years. So it should be possible to complete this by 2030

##### North
* Manchester-Leeds-York total route km is around 100, some of which should be complete by 2025. This team would presumably also work on the Bradford-Pudsey-Leeds line, some 15 route km.

### Remaining main lines in England
#### North
* Leeds-Hull 80 route km, including the spur S from Selby to ECML, and the loop to Hull docks, and bringing electric trains to a further large city.
* Manchester-Sheffield 60 route km
* Manchester-Bradford (Calder) line (65 route km)
* Sheffield N to the E Coast Main Line at S Kirby Jcn some 30 route km
* Mexborough-Doncaster (15 route km)
* Teesside: Northallerton-Sunderland-Newcastle (100 route km), plus branch to Teesport and Redcar 20 route km
* Bradford-Carlisle, some 140 route km

Total route km 500

#### Midlands
* Birmingham-Leicester 60 route km
* Coventry-Nuneaton 20 route km
* Birmingham-Derby 50 route km
* Derby-Stoke 50 route km
* Birmingham-Bristol 100 route km
* Cheltenham-Cardiff 60 route km
* Didcot-Coventry 100 route km
* Grantham-Nottingham 30 route km

Total route km 480

#### E Anglia
* Felixstowe-Ipswich 25 route km; Stowmarket-Ely 60 route km; Ely-Peterborough-Leicester 130 route km
* Norwich-Ely 90 route km

Total route km 300

#### South
* remaining 50 route km from Great Western electrification in Bristol, thereby bringing electric trains to the final large city (much of the preparatory work has already been done)
* Southampton-Bristol 85 route km (stretch near Southampton needs dual supply)
* Basingstoke-Reading 25 route km (needs either Southampton-Basingstoke to be dual supply, or trains with ability to use both)
* Bristol-Plymouth 210 route km
* Newbury-Taunton 165 route km

Total route km 540

#### Grand Total
These lines, plus the freight priorities listed on the Priorities page, total some 2,000 route km, say 4,000 stk. At the *Electrification Cost Challenge*'s 100stk estimate for one team year, a total of 40 team years or 4 teams to get this done 2030-40. Unlike NR's 13,000 stk, this seems doable. For comparison, in GB as a whole some 800 route km have been electrified in the last 10 years.
