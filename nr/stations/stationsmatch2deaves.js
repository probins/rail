/**
 * node stationsmatch2deaves.js
 * load stationsa.csv (etc) and stations.geojson
 * log stationsa not in stations
*/

const fs = require('fs'),
    os = require('os');

const a = fs.readFileSync('./deaves/stationsa.csv', 'utf-8').split('\n');

// read stations file
const stations = JSON.parse(fs.readFileSync('../../public/gis/stations.geojson', 'utf-8')).features;

let notFounds = [], count = 0;

a.forEach((s) => {
  let arr = s.split(',');
    if (stations.find(obj => obj.properties.name === arr[0]) === undefined) {
      // notFounds.push([a.Station,a.ELR]);
      console.log(arr[0], arr[1]);
      count++;
    }
});

// fs.writeFile('stationsnotfound.csv', notFounds.join(os.EOL), function(err) {
//   if(err) {
//     console.log(err);
//   } else {
//     console.log(count + " crs not found");
//   }
// });
