import lume from "https://deno.land/x/lume@v0.15.0/mod.js";
import createData from './tracks/createData.js';

const site = lume({
  src: "src",
  dest: "public",
  prettyUrls: false
});

site.copy("density.png");

function addData() {
  Deno.mkdirSync("public/tracks");
  Deno.chdir("./tracks");
  createData();
}

site.addEventListener("afterBuild", addData);

site.build();
