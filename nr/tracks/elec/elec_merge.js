const fs = require('fs');
const promiseFs = fs.promises;

const elecrecs = fs.readFileSync('./electrified.csv', 'utf-8').split('\r\n');
elecrecs.shift(); // remove header

let elr, elr_recs = [], elr_rec = [], id, start, end, start_ends = [],
    type, mixedType = {};

// wrongly listed as electrified
const wrongs = ['DMY', 'ECN2', 'EKE', 'MLA', 'MRL1', 'STR1'];

elecrecs.forEach((rec) => {
  let arr = rec.split(',');
  // ignore recs for wrongly listed lines
  if (wrongs.includes(arr[2])) {
    return;
  }
  if (parseFloat(arr[4]) != end) {
    // change of sequence
    if (typeof end !== 'undefined') {
      // new seq within elr
      start_ends.push([start, end]);
      if (arr[3] != id) {
        // new id
        elr_rec.push([id, start_ends]);
        start_ends = [];
      }
    }
    id = arr[3];
    start = parseFloat(arr[4]);
  }

  if (arr[2] != elr) {
    // new elr
    if (typeof elr !== 'undefined') {
      // add prev seq to op
      elr_recs.push([elr, type, elr_rec]);
    }
    elr = arr[2];
    elr_rec = [];
    type = getType(arr[8]);
  }

  // lines with mixed electric type
  if (getType(arr[8]) != type) {
    mixedType[arr[2]] = `${getType(arr[8])} ${type}`;
  }

  end = parseFloat(arr[5]);
});

function getType(type) {
  // consolidate elec types
  if (['F','R','S','T'].includes(type)) {
    return 'T';
  }
  if (['A','B','L','O/H LINE'].includes(type)) {
    return 'L'
  }
  return type;
}

// last rec is probably null
if (!!end) {
  start_ends.push([start, end]);
  elr_rec.push([id, start_ends]);
  elr_recs.push([elr, type, elr_rec]);
}

promiseFs.writeFile('elecrecs.json', JSON.stringify(elr_recs))
.then(() => {
  console.log(`${Object.keys(elr_recs).length} elr recs saved`);
})
.catch(err => {
  console.log(err);
});

console.log(`mixed types: ${JSON.stringify(mixedType)}`);
