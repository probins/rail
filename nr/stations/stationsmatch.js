/**
 * node stationsmatch.js
 * load tiplocs.json and stations.geojson
 * output stationsnotfound.csv, for tiploc crs not in stations
*/

const fs = require('fs'),
    os = require('os');

const tiplocs = require('../tiplocs.json');

// read stations file
const stations = JSON.parse(fs.readFileSync('./stations.geojson', 'utf-8')).features;

let notFounds = [], count = 0;

tiplocs.forEach((t) => {
  if (t.crs) {
    if (stations.find(obj => obj.properties.stn_code === t.crs) === undefined) {
      notFounds.push([t.crs,t.name,t.easting,t.northing]);
      count++;
    }
  }
});

fs.writeFile('stationsnotfound.csv', notFounds.join(os.EOL), function(err) {
  if(err) {
    console.log(err);
  } else {
    console.log(count + " crs not found");
  }
});
