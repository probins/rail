/*
 * Class for FeatureCollection functions
 * functions for addFeature, getFeatures, simplifyGeom, createSimplifiedFeature,
 * simplifyAll and writeFile
 */

// simplify-js on npm
import simplify from "https://jspm.dev/simplify-js";

export default class FeatureCollection {
  // create basic FC structure
  constructor() {
    this.fc = {"type": "FeatureCollection", "features": []};
  }
  // param: GeoJSON feature
  addFeature(feature) {
    this.fc.features.push(feature);
  }
  // return: array of GeoJSON features
  getFeatures() {
    return this.fc.features;
  }
  // param: GeoJSON LineString geometry
  // return: simplified coordinates
  simplifyGeom(geom) {
    let coords = [];
    let obj = geom.coordinates.map(point => {return {'x':point[1], 'y':point[0]}});
    let simplified = simplify(obj, 0.0001);
    coords = simplified.map(point => {return [point.y,point.x]});
    return coords;
  }
  // param: GeoJSON LineString feature
  // return: GeoJSON LineString feature with simplified geometry
  createSimplifiedFeature(inFeature) {
    let feature = {
      "type": "Feature",
      "id": inFeature.id,
      "geometry": {
        "type": inFeature.geometry.type
      }
    };
    let geom = inFeature.geometry;
    feature.geometry.coordinates = this.simplifyGeom(geom);
    feature.properties = inFeature.properties;
    return feature;
  }
  // param: array of GeoJSON LineString features
  // simplifies and adds all features in array
  simplifyAll(features) {
    features.forEach(f => {
      let outF = this.createSimplifiedFeature(f);
      this.addFeature(outF);
    });
  }
  // param: file name (including path)
  // writes the Feature Collection to the file
  writeFile(fileName) {
    Deno.writeTextFile(fileName, JSON.stringify(this.fc))
    .then(() => {
      console.log(`${this.fc.features.length} recs saved to ${fileName}`);
    })
    .catch(err => {
      console.log(err);
    });
  }
}
