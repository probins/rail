/*
 * deno run --allow-read --allow-write singletracks.js <elrs>
 *
 * Script to output ELR(s) from nrtracks.geojson as separate geojson file(s),
 * where <elrs> is a comma-separated list of ELR(s) to be output.
 * This helps, for example, working out at which coordinates they should be split.
 * Each ELR file will be named <elr>.geojson.
 */

const singletracks = Deno.args[0].split(',');

const tracks = JSON.parse(Deno.readTextFileSync('./nrtracks.geojson')).features;

singletracks.forEach(elr => {
  let track = tracks.find(t => t.properties.name == elr);

  Deno.writeTextFile(`${elr}.geojson`, JSON.stringify(track))
  .then(() => {
    console.log(`${elr}.geojson saved`);
  })
  .catch(err => {
    console.log(err);
  });
});
